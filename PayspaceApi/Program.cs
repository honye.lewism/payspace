﻿// See https://aka.ms/new-console-template for more information
using Newtonsoft.Json;
using PayspaceApi;
using PayspaceApi.Models;
using RestSharp;
using System.Linq;

var payspace = new Payspace();

var employee = new Employee();
employee.Title = "Mr";
employee.FirstName = "User ";
employee.LastName = "User ";
employee.PreferredName = "users";
employee.EmployeeNumber = "e1100";
employee.Gender = "Male";
employee.Birthday = DateTime.UtcNow;
employee.DateCreated = "";
employee.Initials = "UB";
employee.Nationality = "USA";
employee.CellNumber = "0123456789";
employee.Race = "African";
employee.Addresses = new List<Address>() {
    new Address() {
        AddressLine1 = "#123 Longway RD, Sunningdale, New York, USA",
        EmployeeNumber = "e1100",
        AddressType = "Physical" ,
        AddressCountry="USA",
        //SameAsPhysical=false
    }
};
employee.CustomFields = new List<CustomField>() { new CustomField() { Code = "abc", Value = "123" } };

RestResponse? getEmpl;
try
{
    getEmpl = await payspace.GetMetadataAsync();

    if (!getEmpl.IsSuccessful)
    {
        await payspace.Authenticate();
        await Task.Delay(2000);
        getEmpl = await payspace.GetMetadataAsync();
    }
}
catch (InvalidOperationException ex)
{
    await payspace.Authenticate();
    await Task.Delay(2000);
    getEmpl = await payspace.GetMetadataAsync();
}
Console.WriteLine("token rawanika \n {0}", getEmpl.Content);
