﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayspaceApi.Models
{
    internal class Model
    {
        public int Id { get; set; }

        public string Token { get; set; }
        
        public int CompanyId { get; set; }
        
        public int GroupId { get; set; }

    }
}
