﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayspaceApi.Models
{
	public class Employee
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[System.ComponentModel.DataAnnotations.Key, Column(Order = 0)]
		public int EmployeeId { get; set; }
		public string? EmployeeNumber { get; set; }
		public string? Title { get; set; }
		public string? FirstName { get; set; }
		public string? LastName { get; set; }
		public string? PreferredName { get; set; }
		public string? MaidenName { get; set; }
		public string? MiddleName { get; set; }
		public string? Initials { get; set; }
		public string? Email { get; set; }
		public DateTime Birthday { get; set; }
        public string HomeNumber { get; set; }
        public string WorkNumber { get; set; }
        public string CellNumber { get; set; }
        public string WorkExtension { get; set; }
        public string Language { get; set; }
        public string Gender { get; set; }
        public string MaritalStatus { get; set; }
        public string Race { get; set; }
        public string Nationality { get; set; }
        public string Citizenship { get; set; }
        public string DisabledType { get; set; }
        public bool ForeignNational { get; set; }
        public string DateCreated { get; set; }
        public string EmergencyContactName { get; set; }
        public string EmergencyContactNumber { get; set; }
        public string EmergencyContactAddress { get; set; }
        public bool IsRetired { get; set; }
        public string? CustomFieldValue { get; set; }
        public string? CustomFieldValue2 { get; set; }
        public string? UifExemption { get; set; }
        public string? SdlExemption { get; set; }
        public bool EtiExempt { get; set; }
        public string ImageDownloadUrl { get; set; }
        public virtual ICollection<Address>? Addresses { get; set; }
		public virtual ICollection<CustomField>? CustomFields { get; set; }
	}

	public class CustomField
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[System.ComponentModel.DataAnnotations.Key, Column(Order = 0)]
		public int Id { get; set; }
		public string? Code { get; set; }
		public string? Label { get; set; }
		public string? Value { get; set; } 
	}
	public class Address
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[System.ComponentModel.DataAnnotations.Key, Column(Order = 0)]
		public int AddressId { get; set; }
		public string? AddressType { get; set; }
		public string? EmployeeNumber { get; set; }
		public string? AddressLine1 { get; set; }
		public string? AddressLine2{ get; set; }
		public string? AddressLine3{ get; set; }
		public string? AddressCode { get; set; }
		public string? AddressCountry { get; set; }
		public string? Province { get; set; }
		public string? UnitNumber { get; set; }
		public string? Complex { get; set; }
		public string? StreetNumber { get; set; }
		public bool SameAsPhysical { get; set; }
		public string? IsCareofAddress { get; set; }
		public string? CareOfIntermediary { get; set; }
		public string? SpecialServices { get; set; }
	}
}
