﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayspaceApi.Models
{
    internal class ResponseModel
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key, Column(Order = 0)]
        public int Id { get; set; }
        public String? access_token { get; set; }
        public int expires_in { get; set; }
        public String? token_type { get; set; }
        public String? scope { get; set; } 
        public virtual ICollection<GroupCompany>? group_companies { get; set; }
    }

    public class GroupCompany
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key, Column(Order = 0)]
        public int Id { get; set; }
        public int group_id { get; set; }
        public String? group_description { get; set; }
        public virtual ICollection<Company>? companies { get; set; }
    }
    public class Company
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [System.ComponentModel.DataAnnotations.Key, Column(Order = 0)]
        public int Id { get; set; }
        public int company_id { get; set; }
        public String? company_name { get; set; }
        public String? company_code { get; set; }
    }
}
