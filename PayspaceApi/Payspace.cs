﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using PayspaceApi.Models;
using RestSharp;

namespace PayspaceApi
{
    internal class Payspace
    {
        public async Task<String?> GetTokenAsync()
        {
            RestClient client = new RestClient("https://staging-identity.yourhcm.com/connect/token");
            RestRequest request = new RestRequest();

            request.AddHeader("Content-Type", "application/x-www-form-urlencoded"); 
            request.AddParameter("client_id", "unbsap");
            request.AddParameter("client_secret", "8442f42a-0c52-4802-8214-84fc4cde1cbc");
            request.AddParameter("scope", "api.full_access");

            try
            {
                var response = await client.ExecutePostAsync(request);
                return response.Content;
            }
            catch (Exception exx)
            {
                throw;
            }

        }
        
        public async Task<RestResponse?> GetDetailsAsync(string Field)
        {
            var model = await FileCrud.GetModelAsync();
            RestClient client = new RestClient($"https://apistaging.payspace.com/odata/v1.1/{model.CompanyId}/{Field}");

             RestRequest request = new RestRequest();
            request.AddHeader("Authorization", $"Bearer {model.Token}");  
            request.AddHeader("Content-type", "application/json");  
            //Console.WriteLine("\n get \n"+request);

            try
            {
                var response = await client.ExecuteGetAsync(request);
                  return response;
            }
            catch (Exception exx)
            {
                throw;
            }
        }

        public async Task<RestResponse?> GetMetadataAsync()
        {
            var model = await FileCrud.GetModelAsync();
            RestClient client = new RestClient($"https://apistaging.payspace.com/odata/v1.1/{model.CompanyId}/$metadata");

            RestRequest request = new RestRequest();
            request.AddHeader("Authorization", $"Bearer {model.Token}");
            //request.AddHeader("Content-type", "application/json");
            //Console.WriteLine("\n get \n"+request);

            try
            {
                var response = await client.ExecuteGetAsync(request);
                return response;
            }
            catch (Exception exx)
            {
                throw;
            }
        }

        public async Task<RestResponse> AddEmployee(Employee employee)
        {
            var model = await FileCrud.GetModelAsync();
            RestClient client = new RestClient($"https://apistaging.payspace.com/odata/v1.1/{model.CompanyId}/EmployeeWithAddress");

            RestRequest request = new RestRequest();
            request.AddHeader("Authorization", $"Bearer {model.Token}");
            request.AddHeader("Content-type", "application/json");
            request.AddParameter("application/json", employee, ParameterType.RequestBody);
            return await client.ExecutePostAsync(request);
        }

        public async Task<String?> GetEmployeeDetails()
        {
            var response = await  GetTokenAsync();

            var respo = JsonConvert.DeserializeObject<ResponseModel>(response);
            var token = respo?.access_token;
            var company =respo?.group_companies?.FirstOrDefault();
            return  company?.group_description;
        }

        public async Task Authenticate()
        {
            var token = await GetTokenAsync();

            var response = JsonConvert.DeserializeObject<ResponseModel>(token);

            var models = response.group_companies.Select((gc, i) => new Model
            {
                GroupId = gc.group_id,
                CompanyId = gc.companies.First().company_id,
                Token = response.access_token,
            });

            models.ToList().ForEach(model => FileCrud.Save(model));
        }
    }
}