﻿using Newtonsoft.Json;
using PayspaceApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PayspaceApi
{
    internal class FileCrud
    {
        public const string _ext = ".json";
        private static string baseDir = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
        private static string dir = Path.Combine(baseDir, "Payspace");

        public static async void Save(Model model)
        {
            var filename = Path.Combine(dir, $"{DateTime.Now.ToString("s").Replace(':', '-')}{_ext}");

            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);

            using var stream = new StreamWriter(filename);

            var smodel = JsonConvert.SerializeObject(model);

            await stream.WriteLineAsync(smodel);

        }
         
        public static async Task<Model> GetModelAsync()
        {
            var a = Directory.EnumerateFiles(dir).OrderBy(f => new FileInfo(f).CreationTime).Reverse().Take(1).Single();
            using var stream = new StreamReader(a);
            var b = await stream.ReadToEndAsync();
            return JsonConvert.DeserializeObject<Model>(b);
        }
    }
}
